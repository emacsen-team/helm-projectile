helm-projectile (0.14.0-8) UNRELEASED; urgency=medium

  [ Nicholas D Steeves ]
  * Drop emacs24 from Enhances (package does not exist in bullseye).

  [ Sean Whitton ]
  * Orphan package.
  * Run 'wrap-and-sort -ast'.

  [ Debian Janitor ]
  * Bump debhelper from deprecated 9 to 13.
  * Set debhelper-compat version in Build-Depends.
  * Update renamed lintian tag names in lintian overrides.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Drop unnecessary dh arguments: --parallel

 -- Nicholas D Steeves <sten@debian.org>  Wed, 28 Jul 2021 18:40:43 -0400

helm-projectile (0.14.0-7) unstable; urgency=medium

  * Team upload.
  * Rebuild against dh-elpa 2.1.5.
    Upload pushed to dgit-repos but not salsa.
    See <https://lists.debian.org/debian-emacsen/2024/07/msg00077.html>.

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 25 Jul 2024 22:10:32 +0900

helm-projectile (0.14.0-6) unstable; urgency=medium

  * Team upload

  [ David Krauser ]
  * Update maintainer email address

  [ Dhavan Vaidya ]
  * d/control: Change Vcs-{Browser,Git} URL to salsa.debian.org

  [ David Bremner ]
  * Rebuild with dh-elpa 2.x
  * Remove malformed lintian-overrides

 -- David Bremner <bremner@debian.org>  Mon, 25 Jan 2021 18:23:22 -0400

helm-projectile (0.14.0-5) unstable; urgency=medium

  * Team upload.
  * Regenerate source package with quilt patches

 -- David Bremner <bremner@debian.org>  Sun, 25 Aug 2019 15:29:52 -0300

helm-projectile (0.14.0-4) unstable; urgency=medium

  * Team upload.
  * Rebuild with current dh-elpa

 -- David Bremner <bremner@debian.org>  Sun, 25 Aug 2019 10:19:36 -0300

helm-projectile (0.14.0-3) unstable; urgency=medium

  * Team upload.
  * Rebuild with dh-elpa 1.13 to fix byte-compilation with unversioned
    emacs

 -- David Bremner <bremner@debian.org>  Sat, 02 Jun 2018 20:52:49 -0300

helm-projectile (0.14.0-2) UNRELEASED; urgency=medium

  * Drop dash-el dependency (Closes: #865913).
    Thanks to Mykola Nikishov for reporting the problem.
  * Bump dh-elpa dependency to (>= 1.7) (Closes: #865913).
    Ensures that elpa-dash is included in ${elpa:Depends}.
  * Suggest elpa-helm-ag.

 -- Sean Whitton <spwhitton@spwhitton.name>  Mon, 26 Jun 2017 18:11:45 +0100

helm-projectile (0.14.0-1) unstable; urgency=medium

  * Initial release (new source package split from src:projectile).

 -- Sean Whitton <spwhitton@spwhitton.name>  Sun, 24 Jul 2016 19:42:22 -0700
